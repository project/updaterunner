# Update Runner

This module allows to run any hook_update_N code from any module.
It is for development purposes only.
Do not enable this module on the production server.

## Table of contents

- Installation
- How to use
- Maintainers

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## How to use
1. Go to /admin/config/development/update-runner
2. Explore and run any of the listed hook_update_N

## Maintainers

Current maintainers:

- [grzegorz.bartman](https://www.drupal.org/u/grzegorz.bartman)
- [jaro.2801](https://www.drupal.org/u/jaro.2801)
- [lukasz.tyc](https://www.drupal.org/u/lukasz.tyc)
- [sygnetica](https://www.drupal.org/u/sygnetica)

Supporting organizations:

- [Droptica](https://www.drupal.org/droptica)
