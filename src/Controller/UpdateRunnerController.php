<?php

declare(strict_types = 1);

namespace Drupal\updaterunner\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Update Runner routes.
 */
final class UpdateRunnerController extends ControllerBase {

  /**
   * Constructs a new UpdateRunnerController object.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   */
  public function __construct(
    protected ModuleExtensionList $moduleExtensionList,
    protected LoggerChannelFactoryInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Returns a list of installed modules.
   */
  public function listModules(): array {
    $modules = $this->moduleHandler()->getModuleList();

    foreach ($modules as $module => $data) {
      $rows[] = [
        'module' => [
          'data' => Link::createFromRoute($module, 'update_runner.module', [
            'module' => $module,
          ])->toString(),
        ],
        'update_hooks' => [
          'data' => count($this->getUpdateHooks($module)),
        ],
      ];
    }

    return [
      [
        '#theme' => 'table',
        '#header' => [
          'module' => $this->t('Module'),
          'update_hooks' => $this->t('Update hooks'),
        ],
        '#rows' => $rows ?? [],
      ],
    ];
  }

  /**
   * Returns a list of update hooks for a module.
   */
  public function listModuleUpdates(string $module): array {
    $output = [];

    $output['back_link'] = [
      '#markup' => Link::createFromRoute($this->t('Back to modules list'), 'update_runner.main')->toString(),
    ];

    foreach ($this->getUpdateHooks($module) as $hook_function => $hook) {
      $url = Url::fromRoute('update_runner.run_update', [
        'module' => $module,
        'hook_function' => $hook_function,
      ]);

      $rows[] = [
        'hook' => [
          'data' => $hook_function,
        ],
        'description' => [
          'data' => $hook['description'] ?? '',
        ],
        'operations' => [
          'data' => [
            '#type' => 'operations',
            '#links' => [
              'run' => [
                'title' => $this->t('Run this hook'),
                'url' => $url,
              ],
            ],
          ],
        ],
      ];
    }

    $output['update_hooks'] = [
      '#theme' => 'table',
      '#header' => [
        'hook' => $this->t('Update hook'),
        'description' => $this->t('Description'),
        'operations' => $this->t('Operations'),
      ],
      '#rows' => $rows ?? [],
      '#empty' => $this->t('There are no update hooks available for @module module.', [
        '@module' => $module,
      ]),
    ];

    return $output;
  }

  /**
   * Returns the page title for a module.
   */
  public function modulePageTitle(string $module): TranslatableMarkup {
    return $this->t('Updates for @module', ['@module' => $module]);
  }

  /**
   * Runs an update hook.
   */
  public function runUpdate(string $module, string $hook_function): RedirectResponse {
    $this->moduleHandler()->loadInclude($module, 'install');
    $module_route = $this->redirect('update_runner.module', ['module' => $module]);

    if (!function_exists($hook_function)) {
      $this->messenger()->addError($this->t('Update hook @hook does not exist.', ['@hook' => $hook_function]));
      return $module_route;
    }

    try {
      $sandbox = [];
      call_user_func_array($hook_function, [&$sandbox]);
      $this->messenger()->addMessage($this->t('Update hook @hook executed successfully.', ['@hook' => $hook_function]));
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('An error occurred while executing @hook.', ['@hook' => $hook_function]));
      $this->logger->get('updaterunner')->error($e->getTraceAsString());
    }

    return $module_route;
  }

  /**
   * Returns a list of update hooks for a module.
   */
  protected function getUpdateHooks(string $module): array {
    $update_hooks = [];
    $module_path = $this->moduleExtensionList->getPath($module);
    $filename = sprintf('%s/%s.install', $module_path, $module);

    if (!file_exists($filename)) {
      return $update_hooks;
    }

    $file = file_get_contents($filename);

    if (preg_match_all('/function ' . $module . '_update_(\d+)/', $file, $matches)) {
      foreach ($matches[1] as $update_number) {
        $hook_name = sprintf('%s_update_%s', $module, $update_number);
        $update_hooks[$hook_name]['name'] = $hook_name;
      }
    }

    if (preg_match_all('/\/\*\*(.*?)\*\/\s*function ' . $module . '_update_(\d+)/s', $file, $matches)) {
      foreach ($matches[2] as $index => $update_number) {
        $hook_name = sprintf('%s_update_%s', $module, $update_number);
        if (isset($update_hooks[$hook_name])) {
          $update_hooks[$hook_name]['description'] = $this->processUpdateDescription($matches[1][$index]);
        }
      }
    }

    return $update_hooks;
  }

  /**
   * Returns a processed update description.
   */
  protected function processUpdateDescription(string $description): string {
    $description = trim($description);
    // Remove everything before the first "/**".
    $description = preg_replace('/^.*?\/\*\*/s', '/**', $description);
    // Format the description.
    $patterns = [
      '/^\s*[\/*]*/',
      '/(\n\s*\**)(.*)/',
      '/\/$/',
      '/^\s*/',
    ];
    $replacements = ['', '$2', '', ''];

    return preg_replace($patterns, $replacements, $description);
  }

}
